# REACT QUERY POC - PROJECT
This project is divided into different lessons. Each branch is a specific lesson. 
A simple REST API server made from express JS provides the endpoints.

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
### `yarn start` -> to start the server

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## REACT QUERY DOCUMENTATION:
[https://react-query.tanstack.com/overview](https://react-query.tanstack.com/overview)

## THANKS
- Nibin Thomas
- Rintu Reji John
